﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task8_SkjelinOttosen 
{
    public abstract class CatFamily : Animal, IClimbable
    {
        public int NumPaws { get; set; }
        public int ClimbRange { get; set; }
        public int ClimbSpeed { get; set; }
     
        public CatFamily(string name, string sex, string description) : base(name, sex, description)
        {
            NumPaws = 4;
        }

        public virtual void Climb(int ClimbRange, int ClimbSpeed)
        {
            
        }
    }
}
