﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task8_SkjelinOttosen
{
    public class Cat : CatFamily, IClimbable
    {
        public Cat(string name, string sex, string description) : base(name, sex, description)
        {
            NumPaws = 4;
        }
        public override void Climb(int ClimbRange, int ClimbSpeed)
        {
            Console.WriteLine($"{Name} can climb an object that is {ClimbRange} meters at a speed of {ClimbSpeed} km/t");
            Console.WriteLine();
        }

        public override void PutToRest()
        {
            IsAlive = false;
        }

        public override void Eat()
        {
            Console.WriteLine("Eating mouse.");
        }

        public override void Sleep()
        {
            Console.WriteLine("Sleep on average 15 hours a day.");
        }

        public override string ToString()
        {
            return $"Name: {base.Name}\nSex: {base.Sex}\nDescription: {base.Description}";
        }
    }
}
