﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Task8_SkjelinOttosen
{
    public abstract class Animal : IPutToRestable
    {
        public string Name { get; set; }
        public string Sex { get; set; }
    
        public string Description { get; set; }

        public bool IsAlive { get; set; }

        protected Animal(string name, string sex, string description)
        {
            Name = name;
            Sex = sex;
            Description = description;
            IsAlive = true;
        }

        public abstract void Eat();
        public abstract void Sleep();
        public virtual void PutToRest()
        {
            
        }
    }
}
