﻿using System;
using System.Collections.Generic;

namespace Task8_SkjelinOttosen
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creates animals
            Leopard jimmy = new Leopard("Jimmy","Male", "Jimmy is just Leopard named Jimmy");
            Cat helloKitty = new Cat("Hello Kitty","Female", "Hello Kitty is afictional cat character produced by the Japanese company Sanrio.");
            Cat tomCat = new Cat("Tom Cat","Male", "Tom Cat is a fictional character in Metro-Goldwyn-Mayer's series of Tom and Jerry theatrical animated short films.");
            Horse jollyJumper = new Horse("Jolly Jumper","Male", "Jolly Jumer is horse character in the Franco - Belgian comics series Lucky Luke");

            // Creates the zoo
            Zoo zoo = new Zoo("The Greater Wynnewood Exotic Animal Park");
            zoo.AddAnimal(jimmy);
            zoo.AddAnimal(helloKitty);
            zoo.AddAnimal(tomCat);
            zoo.AddAnimal(jollyJumper);

            while(true)
            {
                try 
                {
                    // Set the Foreground color to white
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Welcome to " + zoo.Name);
                    Console.WriteLine("Press 1 and enter to see all the animals");
                    Console.WriteLine("Press 2 and enter to see animal facts");
                    Console.WriteLine("Press 3 and enter to follow a live horse race");
                    int input = Int16.Parse(Console.ReadLine());
                    // Set the Foreground color to dark yellow
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    if (input == 1)
                    {
                        zoo.PrintAnimalList();
                    }
                    if (input == 2)
                    {
                        tomCat.Climb(50, 10);
                        jimmy.Climb(10, 30);
                    }
                    if (input == 3)
                    {
                        jollyJumper.Race(100, 80, true);
                    }
                }
                catch(FormatException ex)
                {
                    // Set the Foreground color to dark red
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
                catch(Exception ex)
                {
                    // Set the Foreground color to dark red
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }               
            }
        }
    }
}
